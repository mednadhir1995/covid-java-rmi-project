package Cov;

import Cov.Couche1.metier.Analyse;
import Cov.Couche3.service.CovidRemote;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Scanner;


public class Client {
    public static void main(String[] args) {
        try {
            CovidRemote stub = (CovidRemote) Naming.lookup("rmi://localhost:1099/cov/primary");
            System.out.println("            -----------RESULTAT COVID19---------------");
            System.out.println("            ----CONSULTER BILAN PAR CIN ----- >     Entrer 1             ");
            System.out.println("            ----CONSULTER tous les bilans  ----- >  Entrer 2        ");
            System.out.println("            -----ENVOYER un BILAN           ---- >  Entrer 3          ");
            System.out.println("                -----   Quitter            ----- >  Entrer 4        ");
            System.out.println("            **********************************************");


            Scanner input0 = new Scanner(System.in);
            System.out.println("Saisir Votre Choix :");
            String choix = input0.nextLine();
            switch (choix) {
                case "1":
                    Scanner input = new Scanner(System.in);
                    System.out.println("Saisir Votre CIN :");
                    String txt = input.nextLine();
                    List<Analyse> analyse = stub.filterParCin(txt);
                    for (Analyse a : analyse) {
                        System.out.println("Le Porteur de Cin N° : " + a.getCin() +
                                "   Votre Resultat du bilan est : " + a.getResulat());
                    }

                    break;
                case "2":
                    System.out.println("*******************************************");
                    List<Analyse> analyses = stub.listerAnalyse();
                    for (Analyse an : analyses) {
                        System.out.println("ID : " + an.getIdAnlyse() + " Resultat du Bilan est " + an.getResulat() + "   CIN: " + an.getCin());
                    }
                    System.out.println("*******************************************");
                    break;
                case "3":
                    Scanner input2 = new Scanner(System.in);
                    System.out.println("Saisir id bilan :");
                    String txt2 = input2.nextLine();
                    Scanner input3 = new Scanner(System.in);
                    System.out.println("Saisir le resultat du bilan : :");
                    String txt3 = input3.nextLine();
                    Scanner input4 = new Scanner(System.in);
                    System.out.println("Saisir le CIN Du Citoyen :");
                    String txt4 = input4.nextLine();
                    stub.send(txt2, txt3, txt4);
                    System.out.println("Bilan sauvegardé");
                    break;
                case "4":
                    // code block
                    break;

                default:
                    System.out.println("serveur introuvable");
            }


        } catch (RemoteException | MalformedURLException | NotBoundException e) {
            e.printStackTrace();
        }


        System.out.println("*******************************************");
        System.out.println("-----------RESULTAT COVID19---------------");
    }
}
