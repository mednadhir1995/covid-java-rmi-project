package Cov.Couche3.service;

import Cov.Couche1.metier.Analyse;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface CovidRemote extends Remote {
    // afficher la liste des analyses
    public List<Analyse> listerAnalyse() throws RemoteException;

    // Récupérer des données selon le cin
    public List<Analyse> filterParCin(String c) throws RemoteException;

    // Envoyer les données d'une analyse pour les mettres dans le catalogue
    public void send(String a, String b, String c) throws RemoteException;
}
