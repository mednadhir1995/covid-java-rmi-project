package Cov.Couche3.service;

import Cov.Couche1.metier.Analyse;
import Cov.Couche1.metier.AnalyseCatalogue;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
//import java.util.Scanner;

public class CovidImplementation extends UnicastRemoteObject implements CovidRemote {
    public AnalyseCatalogue AN = new AnalyseCatalogue();

    public CovidImplementation() throws RemoteException {
        super();

    }

    @Override
    public List<Analyse> listerAnalyse() throws RemoteException {
        return AN.ConsulterAnalyse();
    }

    @Override
    public List<Analyse> filterParCin(String mc) throws RemoteException {
        return AN.getAnalyseParCin(mc);
    }

    @Override
    public void send(String a, String b, String c) throws RemoteException {
        Analyse anls = new Analyse(a, b, c);
        AN.envoieAnalyse(anls);
    }





/*
    @Override
    public void send() throws RemoteException {
        Scanner input = new Scanner(System.in);
        System.out.println("What is the book called?..");
        String a = input.nextLine();
        System.out.println("Who is the author?..");
        String b = input.nextLine();
        System.out.println("How many do you have?..");
        String c = input.nextInt();
        AN.envoie(a, b, c);
    }
    */

}
