package Cov.Couche1.metier;

import java.io.Serializable;

public class Analyse implements Serializable {

    private String idAnlyse;
    private String resulat;
    private String cin;

    public Analyse(String idAnlyse, String resulat, String cin) {
        super();
        this.idAnlyse = idAnlyse;
        this.resulat = resulat;
        this.cin = cin;
    }

    public Analyse() {
        super();
    }

    public String getIdAnlyse() {
        return idAnlyse;
    }

    public void setIdAnlyse(String idAnlyse) {
        this.idAnlyse = idAnlyse;
    }

    public String getResulat() {
        return resulat;
    }

    public void setResulat(String resulat) {
        this.resulat = resulat;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }
}
