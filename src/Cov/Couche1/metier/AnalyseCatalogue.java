package Cov.Couche1.metier;

import java.util.ArrayList;
import java.util.List;

public class AnalyseCatalogue {
    private List<Analyse> Analyses = new ArrayList<>();

    public AnalyseCatalogue() {
        Analyses.add(new Analyse("0001", "Negative", "1111"));
        Analyses.add(new Analyse("0002", "Positive", "2111"));
        Analyses.add(new Analyse("0003", "Negative", "3111"));
        Analyses.add(new Analyse("0004", "Negative", "4111"));
        Analyses.add(new Analyse("0005", "Non_concluant", "5111"));


    }

    public List<Analyse> ConsulterAnalyse() {
        return Analyses;
    }

    public List<Analyse> getAnalyseParCin(String cin) {
        List<Analyse> anls = new ArrayList<Analyse>();
        for (Analyse an : Analyses) {
            if (an.getCin().equals(cin))
                anls.add(an);

        }
        return anls;

    }

    public void envoieAnalyse(Analyse newAnalyse) {
        Analyses.add(newAnalyse);
    }


}
