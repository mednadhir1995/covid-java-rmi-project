package Cov.Couche2.serveur;

import Cov.Couche3.service.CovidImplementation;

import java.net.MalformedURLException;
import java.rmi.registry.LocateRegistry;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class ServeurRMI {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);

            //Primary service
            CovidImplementation primary = new CovidImplementation();
            System.out.println(primary.toString());
            Naming.rebind("rmi://localhost:1099/cov/primary", primary);

            //Backup service
            CovidImplementation backup = new CovidImplementation();
            System.out.println(backup.toString());
            Naming.rebind("rmi://localhost:1099/cov/backup", backup);

            // Kill primary service after 15 Seconds
            primary = null;
           // System.gc();
           // primary.finalize();

        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
